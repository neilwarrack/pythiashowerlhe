#!/usr/bin/env bash


# uncomment for full verbitim bash output (it's a lot):
#set -x


###########################################################
# DESCRIPTION:                                            #
#                                                         #
# This bash script will shower one LHE file which sits    #
# alone in a $TARGET_DIR which is an absolute path to be  #
# specified by the user on the command line.              #
#                                                         #
# $TARGET_DIR must contain ONLY ONE LHE FILE!             #
#                                                         #
# If the LHE file is gzipped then it will be unzipped.    #
#                                                         #
# The LHE file will be showered using Pythia8 and events  #
# are sent directly through a Rivet analysis specified on #
# the command line.                                       #
#                                                         #
# The output will be in the form:                        #
#                                                         #
#    output/TimeStamp_RivetAnalsysis_usefulName.yoda      #
#                                                         #
# Where "usefulName" will be the name of the LHE file     #
# unless the LHE file is named (as is often the case with #
# MadGraph) unweighted_events.lhe -- in which case it will#
# be named after the $TARGET_DIR .                        #
#                                                         #
###########################################################




##########################################################
# FUNCTIONS FOR LOGGING ---------------------------------#
##########################################################
note(){                                                  #
    text="${*}"                                          #
    echo "${text}"                                       #
    log_file="log.txt"                                   #
    echo "${text}" >> "${log_file}"                      #
}                                                        #
##########################################################
terminate_time(){                                        #
    # prerequisite_functions: note                       #
    note ">>> Exiting with code \"1\" at:"               #
    note "$(date "+%A %d %B %H:%M:%S %Z %Y (%Y-%m-%dT%H%M%S)")"
    exit 1                                               #
}                                                        #
##########################################################
finish_time(){                                           #
    # prerequisite_functions: note                       #
    note ""                                              #
    note ">>> Finishing at:"                             #
    note "$(date "+%A %d %B %H:%M:%S %Z %Y (%Y-%m-%dT%H%M%S)")"
    exit 0                                               #
}                                                        #
##########################################################



# Setup a new empty log file (preserve old logs)
if [ -f log.txt ]
then
    OLD_FIRST_LINE=$(head -n 1 log.txt)
    OLD_TIMESTAMP=$(echo $OLD_FIRST_LINE | cut -d "(" -f2 | cut -d ")" -f1)
    
    #echo $OLD_TIMESTAMP
    
    if [ -d logs ]
    then
	mv log.txt logs/log_$OLD_TIMESTAMP.txt
    else
	mkdir logs
	mv log.txt logs/log_$OLD_TIMESTAMP.txt
    fi
    
    # Print time to terminal and log.txt
    note "Start time: "$(date "+%A %d %B %H:%M:%S %Z %Y (%Y-%m-%dT%H%M%S)")""
    note ">>> Moved log created at $OLD_TIMESTAMP to $PWD/logs/log_$OLD_TIMESTAMP.txt"
else
    note "Start time: "$(date "+%A %d %B %H:%M:%S %Z %Y (%Y-%m-%dT%H%M%S)")""  
fi
    

# Friendly warning for all:
note ""
note ">>> WARNING: This script will delete any Rivet.yoda, $RIVET_ANALYSIS.yoda,"
note ">>> RivetAnalysis.so file kept in the project directory pythiashowerlhe/"
note ">>> To keep these things safe you should store them in the"
note ">>> analysis/YOUR_ANALYSIS/ directory where nothing will be written"
note ">>> to (only copied from)."
note ""


# Check for correct user input format
if [ $# -eq 4 ]
then

    export RIVET_ANALYSIS="$1"
    export TARGET_DIR="$2"
    export NEVENTS="$3"
    export COMPILEMODE="$4"
    
# Write out user arguments    
    note ">>> Compile mode = $COMPILEMODE"
    note ">>> Target directory (should be an absolute path) = $TARGET_DIR"
    note ">>> No. of events requested (0 = all events) = $NEVENTS"
    note ">>> Rivet Analysis = $RIVET_ANALYSIS"  
    note ""
    note ">>> $PWD/bin/powershower.sh will now attempt to shower a simgle LHE file"
    note ">>> through Pythia8 directly into the rivet analysis specified above"
    note ">>> which should be a complied plugin:"
    note ">>> $PWD/analyses/$RIVET_ANALYSIS/RivetAnalysis.so"
    note ">>> if compile mode is off, or source code:"
    note ">>> $PWD/analyses/$RIVET_ANALYSIS/$RIVET_ANALYSIS.cc"
    note ">>> if compile mode is on."
    note ""

else

    note ">>> ERROR: incorrect amount of inputs!"
    note "Usage:"
    note ""
    note " $ ./bin/powershower.sh X Y Z N"
    note ""
    note "       X = RIVET_ANALYSIS_NAME"
    note "       Y = ABSOLUTE_PATH_TO_LHE_FILE"
    note "       Z = NUMBER_OF_EVENTS_YOU_WANT_TO_SHOWER (0 = for all events)"
    note "       N = \"on\"/\"off\"  (turns on/off rivet-buildplugin feature"
    note "           which will attempt to compile the RivetAnalysis.so plugin"
    note "           from souce code:"
    note "           analyses/RIVET_ANALYSIS_NAME/RIVET_ANALYSIS_NAME.cc"
    note "           If this is \"off\" then RivetAnalysis.so should already"
    note "           exist here:"
    note "           analyses/RIVET_ANALYSIS_NAME/RivetAnalysis.so"
    note ""
    note "e.g.:"
    note ""
    note "$ ./bin/powershower.sh ATLAS_2014_I1304289 absolute/path/to/lhe 2000000 on"
    note ""
    temitate_time
fi


# Check plugin exists in compiled form if COMPILEMODE=off
if [ $COMPILEMODE == "off" ]
then 
    if  [ -f analyses/$RIVET_ANALYSIS/RivetAnalysis.so ]
    then
	# copy analysis to base directory
	note ">>> Copying $PWD/analyses/$RIVET_ANALYSIS/RivetAnalysis.so to working directory: $PWD"
	cp -n $PWD/analyses/$RIVET_ANALYSIS/RivetAnalysis.so . 2>&1 | tee -a log.txt
	
    else
	note ">>> ERROR: No compiled library RivetAnalysis.so found in:"
	note ">>> $PWD/analyses/$RIVET_ANALYSIS/"
	note ">>> turn rivet-buildplugin \"on\" (4th arg to ./bin/powershower.sh)"
	note ">>> and make sure the source code exists:"
	note ">>> $PWD/analyses/$RIVET_ANALYSIS/$RIVET_ANALYSIS.cc"
	temitate_time
    fi
fi


# Build plugin if COMPILEMODE=on
if [ $COMPILEMODE == "on" ]
then
    
    # Check for rivet
    note ">>> Compile mode on (rivet-buildplugin $RIVET_ANALYSIS):"
    note ">>> Look for rivet:"
    if type rivet-buildplugin &> /dev/null
    then
	note ">>> ...rivet-buildplugin exists!"


	# check for rivet source code
	FOUND_RIVET_SOURCE_CODE=0
	note ">>> Look for $RIVET_ANALYSIS.cc in $PWD/analyses/$RIVET_ANALYSIS/"
	if  [ -f analyses/$RIVET_ANALYSIS/$RIVET_ANALYSIS.cc ]
	then
	    note ">>> ...$RIVET_ANALYSIS.cc exists!"
	    FOUND_RIVET_SOURCE_CODE=1
	    note ">>> Executing command: rivet-buildplugin analyses/$RIVET_ANALYSIS/$RIVET_ANALYSIS.cc"
	    note ""

	    rivet-buildplugin analyses/$RIVET_ANALYSIS/$RIVET_ANALYSIS.cc 2>&1 | tee -a log.txt

	    if [ -f RivetAnalysis.so ]
	    then
		note ">>> RivetAnalysis.so created"
		cp RivetAnalysis.so $PWD/analyses/$RIVET_ANALYSIS/ 2>&1 | tee -a log.txt
	    else
		note ">>> Something went wrong building RivetAnalysis.so from $RIVET_ANALYSIS"
		terminate_time
	    fi
	fi
	
    else
	# Abort if no no rivet installed
	note ">>> WARNING: rivet-buildplugin does not exist. Cannot build library."
	terminate_time
    fi       
fi


# Generate a fifo if it doesn't already exist
if [ ! -p fifo.hepmc ]
then
    if [ ! -f fifo.hepmc ]
    then
	mkfifo fifo.hepmc
	note ">>> fifo pipe generated!"
    else
	note ">>> WARNING: There is a FILE called fifo.hepmc, please delete it because it's getting in the way of creating a PIPE called fifo.hepmc."

	# Clean up and terminate
	rm RivetAnalysis.so >> log.txt 2>&1
	terminate_time
    fi
fi


# search for analysis reference yoda file
if  [ -f analyses/$RIVET_ANALYSIS/$RIVET_ANALYSIS.yoda ]
then
    note ">>> found:  $PWD/analyses/$RIVET_ANALYSIS/$RIVET_ANALYSIS.yoda"
    note ">>> copying analyses/$RIVET_ANALYSIS/$RIVET_ANALYSIS.yoda to working directory: $PWD"
    cp analyses/$RIVET_ANALYSIS/$RIVET_ANALYSIS.yoda . >> log.txt 2>&1
else
    note ">>> no file $PWD/analyses/$RIVET_ANALYSIS/$RIVET_ANALYSIS.yoda"
    note ">>> This may cause rivet problems if it's needed...continue anyway..."
fi


# check for pythia command card - required!
if [ ! -f cmd/showerLHE.cmd ]
then
    note ">>> No cmd/showerLHE.cmd file found. This is needed!"

    # Clean up and terminate
    rm RivetAnalysis.so $RIVET_ANALYSIS.yoda >> log.txt 2>&1
    if [ -f $RIVET_ANALYSIS.yoda ]
    then
	rm $RIVET_ANALYSIS.yoda >> log.txt 2>&1
    fi
    terminate_time
fi


# check for pythia showering binary - required!
if [ ! -f bin/showerLHE ]; then
    note ">ERROR>>> No bin/showerLHE binary file found."
    note ">ERROR>>> It can be compiled from src/showerLHE.cc yourself..."
    note ">ERROR>>> ... or, if on Glasgow's PPE system, it can be compiled"
    note ">ERROR>>> by running ./setupPythiaShower.sh"

    # Clean up and terminate
    rm RivetAnalysis.so $RIVET_ANALYSIS.yoda >> log.txt 2>&1
    if [ -f $RIVET_ANALYSIS.yoda ]
    then
	rm $RIVET_ANALYSIS.yoda >> log.txt 2>&1
    fi
    terminate_time
fi



# Create temp pythia cmd file to use
cp cmd/showerLHE.cmd cmd/showerLHE.cmd.temp >> log.txt 2>&1
note ">> Temporary file cmd/showerLHE.cmd.temp created"

# Update number of events (use 0 for all LHEF events in the input file)
note ">>> $NEVENTS events requested."
if sed -i '/^Main:numberOfEvents/s/=.*//' cmd/showerLHE.cmd.temp >> log.txt 2>&1; then
    note ">>> cmd/showerLHE.cmd.temp previous 'number of events' removed"
else
    note ">WARNING>>> something's not good in updating showerLHE.cmd.temp (remove number of events)"
    terminate_time
fi


# Append new number of events in temp pythia command file
export SEDARG_N=/^Main:numberOfEvents/s/$/=\ $NEVENTS/
if sed -i "$SEDARG_N" cmd/showerLHE.cmd.temp >> log.txt 2>&1; then
    note ">>> cmd/showerLHE.cmd.temp new number of events ($NEVENTS) apended"
else
    note ">WARNING>>> something's not good in updating showerLHE.cmd.temp (append number of events)"
    terminate_time
fi

note ">>> Starting loop over LHE files in directory lhe/ for Rivet analysis $RIVET_ANALYSIS"


# set RIVET_ANALYSIS_PATH
note ">>> Setting rivet variable RIVET_ANALYSIS_PATH=$PWD"
export RIVET_ANALYSIS_PATH=$PWD


# Unzip the LHE file if needed
if [ -f $TARGET_DIR/*.lhe.gz ]
then
    note ">>> gzipped LHE file found (of form: foo.lhe.gz)."
    export LHE_FILENAME_GZ=$(echo "$(ls $TARGET_DIR/*.lhe.gz | xargs -n1 basename)")
    note ">>> gunzipping $LHE_FILENAME_GZ..."
    gunzip $TARGET_DIR/$LHE_FILENAME_GZ >> log.txt 2>&1
    if [ ! -f $TARGET_DIR/${LHE_FILENAME_GZ//.gz} ]
    then
	note ">WARNING>>> Something went wrong unzipping file $TARGET_DIR/$LHE_FILENAME_GZ. Abort..."
	terminate_time
    else
	note ">>> ...gunzip successfull!"
	
    fi
fi


# Check there is exactly one LHE file present
if [ `ls -1 $TARGET_DIR/*.lhe 2>/dev/null | wc -l ` != 1 ]
then
    note ">>> TARGET_DIR=$TARGET_DIR contains more than or less than one LHE file."
    note ">>> There should only be one LHE file in TARGET_DIR=$TARGET_DIR"
    note ">>> Abort..."
    terminate_time
else
	# record the LHE filename
    export LHE_FILENAME=$(echo "$(ls -1 $TARGET_DIR/*.lhe | xargs -n1 basename)")
fi


    # find a unique name for the output
if [ -f $TARGET_DIR/unweighted_events.lhe ]
then
#keep everything after the last slash of the path TARGET_DIR
    export LHE_ID=${TARGET_DIR##*/}
else
#name it after the non-standard filename
    export LHE_ID=${LHE_FILENAME//.lhe}
fi


    # Update the pythia cmd file based on location of LHE file
note ">>> path to LHE file: $TARGET_DIR/$LHE_FILENAME"

    # update slashes in path for use with sed
    # (ie "/" -> "\/" so it can be understood)
export TARGET_DIR_FOR_SED=$(echo "$TARGET_DIR" | sed -r 's/\//\\\//g')

    # define argument for sed which is an puts and absolute path to
    # the LHE file at the end of a line beginning with "Beams:LHEF"
export SEDARG_PATH=/^Beams:LHEF/s/$/=\ $TARGET_DIR_FOR_SED\\\/$LHE_FILENAME/
	    

    # Remove previous LHE path in temp pythia command file
if sed -i '/^Beams:LHEF/s/=.*//' cmd/showerLHE.cmd.temp >> log.txt 2>&1; then
    note "--- cmd/showerLHE.cmd.temp previous path to LHE file removed"
else
    note ">WARNING>>> something's not good in updating showerLHE.cmd.temp (remove LHE path)"
    termiate_time
fi

    # Append new LHE path in temp pythia command file
if sed -i "$SEDARG_PATH" cmd/showerLHE.cmd.temp >> log.txt 2>&1
then
    note "--- cmd/showerLHE.cmd.temp new path to LHE file apended"
else
    note ">WARNING>>> something's not good in updating showerLHE.cmd.temp (append LHE path)"
    terminate_time
fi


    # preserve any old rivet logs (the rivet terminal output)
if [ -f rivet.log ]
then
    OLD_RIVET_LOG_FIRST_LINE=$(head -n 1 rivet.log)
    
    OLD_RIVET_TIMESTAMP=$(echo $OLD_RIVET_LOG_FIRST_LINE | cut -d "(" -f2 | cut -d ")" -f1)
    
    if [ -d logs ]
    then
	mv rivet.log logs/$OLD_RIVET_TIMESTAMP\_rivet.log
    else
	mkdir logs
	mv rivet.log logs/$OLD_RIVET_TIMESTAMP\_rivet.log
    fi
    
    
    note "--- Moved rivet.log created at $OLD_RIVET_TIMESTAMP to $PWD/logs/$OLD_RIVET_TIMESTAMP_rivet.log"
fi
    
note "--- Initialising new rivet.log file"
echo "LHE->Pythia->Rivet start time: "$(date "+%A %d %B %H:%M:%S %Z %Y (%Y-%m-%dT%H%M%S)")"" >> rivet.log 2>&1
note ""
    
    
    # Do Pythia and Rivet
note "--- Starting LHE -> PYTHIA -> RIVET process..."
note ""

    ########################################################
./bin/showerLHE cmd/showerLHE.cmd.temp fifo.hepmc &
rivet -a $RIVET_ANALYSIS fifo.hepmc 2>&1 | tee -a rivet.log
    ########################################################

    #record time for logs
TS=$(date "+%m%d%H%M")

    # Make an output directory if it doesn't exist
if [ ! -d output ]; then mkdir output; fi

    # Save the Rivet.yoda file as something sensible
if [ -f Rivet.yoda ]
then
    export YODA_NAME=$TS\_$RIVET_ANALYSIS\_$LHE_ID.yoda
    note "--- saving yoda file output as:"
    
    if cp Rivet.yoda $PWD/output/$YODA_NAME >> log.txt 2>&1
    then
	note "--- $PWD/output/$YODA_NAME"
    else
	note ">ERROR>>> something went wrong when trying to save yoda output. Terminating to avoid loss of yoda file."
	terminate_time
    fi
fi


    # advise on ploting
note ""
note ">>> To plot these results do:"
note ""
note " $ cd $PWD/analyses/$RIVET_ANALYSIS"
note " $ source ../../setupPythiaShower.sh"
note " $ export RIVET_ANALYSIS_PATH=\$PWD"
note " $ rivet-mkhtml ../../output/$YODA_NAME"
note ""
note ">>> BUT BE CAREFUL!"
note ">>> It overwrite any already existing directory:"
note ">>> $PWD/analysis/$RIVET_ANALYSIS/rivet-plots"
note ""


# Clean up relic yoda file, temp file and Rivet Plugin:
# (NB the all-important Rivet.yoda file is copied (with a time-stamp
# and 'analysis name') to pythiashowerlhe/output
note ">>> tidying up directory: $PWD"
rm $RIVET_ANALYSIS.yoda >> log.txt 2>&1
rm ./cmd/showerLHE.cmd.temp >> log.txt 2>&1
rm RivetAnalysis.so >> log.txt 2>&1
rm Rivet.yoda >> log.txt 2>&1

finish_time

