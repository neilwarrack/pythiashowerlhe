#!/usr/bin/env bash

# To setup and use the code in this project type:
#
#   source setupPythiaShower.sh
#
# For more help type:
#
#   ./setupPythiaShower.sh -h


if [[ "$ALREADYSETUP" != "good_to_go" ]] 
then

    export LOCAL_DIR=/home/ppe/n/nwarrack/local
    export PATH=$LOCAL_DIR/MG5_aMC_v2_6_3_2/bin/:$LOCAL_DIR/bin:$PATH
    export LIBRARY_PATH=$LOCAL_DIR:$LOCAL_DIR/lib:$LD_LIBRARY_PATH
    export LD_LIBRARY_PATH=$LOCAL_DIR:$LOCAL_DIR/lib:$LD_LIBRARY_PATH
    export CPATH=$LOCAL_DIR/include:$CPATH
    
    export ALREADYSETUP="good_to_go"
fi


if [ ! "$1" == "-p" ]
then
    if [ -d ./bin ]
    then
	if [ ! -f ./bin/showerLHE ]
	then
	    
	    # build object and binary file
	    g++ -c src/showerLHE.cc
	    g++ showerLHE.o -lpythia8 -lHepMC -ldl -o bin/showerLHE
	    
	    # Remove relic object file
	    rm showerLHE.o
	    
	    # Run test (should produce: "-->showerLHE compiled and operational")
	    ./bin/showerLHE testshowerLHE
	    
	fi
    else
	echo "to compile ./binshowerLHE you must run setupPythiaShower.sh in the working directory which contains ./bin and ./src"
    fi
fi

# do ./setupPythiaShower.sh -h to see this message:
if [[ "$1" == "-h" ]]; then
    echo "INFO:"
    echo ""
    echo "- This project will take LHE files and shower them using Pythia8."
    echo "- The main program is a large bash script called bin/powershower.sh"
    echo "- Instead of producing the resulting (large) hepmc evnt records it"
    echo "  will pipe the events directly into a supplied rivet analysis."
    echo "- The rivet routine can be compiled for you."
    echo "- This project is designed for use by the TopFitter collaboration"
    echo "  using the PPE hex machine which has access to:"
    echo "    /home/ppe/n/nwarrack/local"
    echo "  ...where the necesary bins and libs live."
    echo ""
    echo "__User guide:__"
    echo ""
    echo ""
    echo "  1) INPUT:"
    echo ""
    echo "   1.1) There must exist LHE files in one"
    echo "        of the following formats:"
    echo ""
    echo "          a) weighted_events.lhe"
    echo "          b) weighted_events.lhe.gz"
    echo ""
    echo "   1.2) The LHE files must exist in the"
    echo "        following locations:"
    echo ""
    echo "           LHE/yourLHEs/dir1/weighted_events.lhe(.gz)"
    echo "           LHE/yourLHEs/dir2/weighted_events.lhe(.gz)"
    echo "           LHE/yourLHEs/dir3/weighted_events.lhe(.gz)"
    echo "           ... etc ...etc"
    echo ""
    echo ""
    echo "  2) PROCESS:"
    echo ""
    echo "   2.1) To process your LHE files you will need the rivet" 
    echo "        analysis source code for the routine that you"
    echo "        want to shower events through. It must be kept"
    echo "        in the analyses/ directory (along with it's"
    echo "        reference yoda file). i.e. the following files"
    echo "        must exist:"
    echo ""
    echo "           analyses/RIVET_ANALYSIS/RIVET_ANALYSIS.cc"
    echo "           analyses/RIVET_ANALYSIS/RIVET_ANALYSIS.yoda"
    echo ""
    echo ""
    echo "   2.2) ALL the LHE files in \"yourLHEs/\" will be"
    echo "        showered through the provided rivet routine. If"
    echo "        the compiled rivet plugin (usually called" 
    echo "        RivetAnalysis.so) exists then it should live in"
    echo "        with it's analysis here:"
    echo ""
    echo "           analyses/RIVET_ANALYSIS/RivetAnalysis.so"
    echo ""
    echo "        if it doesn't powershower.sh will try to compile it."
    echo ""
    echo ""
    echo "  3) USAGE:"
    echo ""
    echo "   3.1) To use powershower.sh you should run this script first"
    echo "        to set up some necesary paths for rivet, pythia, etc."
    echo ""
    echo "   3.2) It should be used from this main directory. Here is an"
    echo "        example of how you could use it for the first 1000"
    echo "        events in LHE files with >1000 events with the"
    echo "        analysis:"
    echo "        analysis/ATLAS_2017_I1495243/ATLAS_2017_I1495243.cc"
    echo ""
    echo "            ./bin/powershower.sh ATLAS_2017_I1495243 1000"
    echo ""
    echo ""
    echo "  4) COMPLAINTS:"
    echo ""
    echo "        complaints to the complaints department:"
    echo "        neil.warrack@cern.ch"
    echo ""
fi
