# Welcome!
This project, ***pythiashowerlhe***, contains code which can (with minimal effort) shower multiple LHE files through a rivet analysis producing *yoda file* outputs - one yoda file for each LHE file.

# Prerequisites:
**option 1:** Rivet and Pythia8
**option 2:** Access to Glasgow Universities PPE cluster

# Usage:
```
source setupPythiaShower.sh
./bin/powershower.sh X Y Z N
```

*(The arguments X, Y, Z & N allow you to specify which rivet analysis to use, the path to your LHE files, the number of events you want to shower and whether you want to compile your rivet plugin from source code or use an already compiled plugin)*
# The purpose:
**The primary purpose:**
The code was developed so that we could shower pre-made and stored LHE files using Pythia8 straight into the Rivet analysis without producing the (sometimes extremely large!) hepmc event records in the process.
**The secondary (more developed and more detailed) purpose:**
We wanted a simple one-line command that could compile a rivet analysis, then shower multiple (possibly gzipped) LHE files through it and store the resulting yoda files logically.

# Logs:
The bash script (bin/powershower.sh) creates it's own logs which are archived on a following use of the script. The logs are renamed with 'time-stamp' titles. This way the user doesn't need to worry about moving or sorting them. Yes, this can result in a pile up of useless logs but you can purge them if and when you need. Every execution of rivet (happens as many times as there LHE files to loop over) creates it's own rivet-log, which is archived in a similar manner.

# Keep the playground tidy:
The .gitignore file is written so that usage of the code will not get in the way
of version control (see .gitgnore file). 

# Instructions:

A guide to running the code is in setupPythiaShower.sh which can be accesed with
```
$ ./setupPythiaShower.sh -h
```
There are readme files in some directories to explain their purpose and there is a fuller example of how to download and use the script in exampleRun.sh
# Running on a batch system:
We wanted to use a condor batch system and there is a **condor/** directory with some info and scripts to allow you to do that.
# Anything else?
Please contribute, make suggestions, report bugs.